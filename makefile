all: server client

CC = gcc
OBJECTS1=server.o
OBJECTS2=client.o
SOURSES1=server.c 
SOURSES2=client.c
PATH1=src/
PATH2=build/src/
PATH3=bin/
EXC1=server
EXC2=client

server: 
	$(CC) $(PATH1)$(SOURSES1) -o $(PATH3)$(EXC1) 

client:
	$(CC) $(PATH1)$(SOURSES2) -o $(PATH3)$(EXC2)

clean:
	rm -rf $(PATH3)*

rebuild:
	$(MAKE) clean
	$(MAKE) all

checkdir:
	mkdir -p bin build build/src
